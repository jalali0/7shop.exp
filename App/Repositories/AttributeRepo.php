<?php
namespace App\Repositories;
use App\Models\Attribute;

class AttributeRepo extends BaseRepo {
    protected $model = Attribute::class;

}