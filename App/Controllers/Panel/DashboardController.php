<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class DashboardController{

    public function index($request) {
        $data = [];

        View::load('panel.dashboard.index',$data,'panel-admin');
    }

}