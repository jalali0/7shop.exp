<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model {
	protected $table ;
	protected $primaryKey ;
	public $timestamps = false ;
    protected $guarded = ['id'];


}